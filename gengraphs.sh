#!/bin/bash

# Requirement:
# -python 3.x
# -gnuplot 5.x
# -ImageMagick

for b in python3 convert gnuplot ; do
    which $b > /dev/null 2>&1
    if [ $? -ne 0 ] ; then
        echo "$b is a required binary, please install that first." >&2
        exit 1
    fi
done

if [[ "$(which python3)" =~ ^(/usr(/local)?)?/bin/python ]] ; then
    echo "You're not running in a virtual environment."
    echo -n "Create one? (Y/N) (or terminate with Crtl-C) "
    while : ; do
        read ANS
        [ -z "$ANS" ] && {
            echo Exiting upon request
            exit 0
        }
        if [[ "${ANS,,}" =~ ^y ]] ; then
            TMPENV=$(mktemp -u)
            echo "Installing virtual environment"
            python3 -m venv $TMPENV
            if [ $? -ne 0 ] ; then
                echo "Failed creating virtual environment" >&2
                exit 1 
            fi
            source ${TMPENV}/bin/activate
            break
        elif [[ "${ANS,,}" =~ ^n ]] ; then
            break
        else
            unset ANS
            echo -n "Please anser Y(es) or N(o): "
        fi
    done
fi

if [ -z $(which pip3 2>/dev/null) ] ; then
    echo "Please install pip3 to install required modules" >&2
    exit 1
fi
pip3 install -r requirements.txt
if [ $? -ne 0 ] ; then
    echo "Could not install all required python modules" >&2
    exit 1
fi

# Run script
python3 cherrypick.py
for i in *-vars.txt ; do
    [ "$i" = alltime-vars.txt ] && continue
    export SUBSET="${i%-vars.txt}"
    gnuplot cherrypick.gpl
    convert ${SUBSET}-regression.svg ${SUBSET}-regression.png
    convert ${SUBSET}-timeline.svg ${SUBSET}-timeline.png
done
echo -e "\nAll graphs are generated\n"
