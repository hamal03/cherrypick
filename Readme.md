Cherrypick
==========

This repo contains the bash, python and gnuplot scripts to generate the graphs
for the Medium article "Backtesting Bitcoin's Stock to Flow Model".

To recreate the graphs, run the "gengraphs.sh" bash script. It will check the
dependencies, run the python script to create the proper data files and then
run gnuplot for each data set that is defined in the python script. It is
advisable to run the python script in a virtual environment. If that is not
detected, the shell script will opt to create one, activate it and then run the
python script. If you reject creating the virtual environment, the required
modules will be created in your primary environment.
